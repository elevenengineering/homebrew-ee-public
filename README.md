# Eleven Engineering Public Homebrew Repository

To install the Eleven Engineering XInC2 tools, run the following after installing Homebrew:

```
brew tap user/elevenengineering https://bitbucket.org/elevenengineering/homebrew-ee-public
brew install sxc
```
