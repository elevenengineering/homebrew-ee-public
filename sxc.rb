class Sxc < Formula
  desc "XInC2 Toolchain"
  homepage "https://www.elevenengineering.com"

  url "https://bitbucket.org/elevenengineering/homebrew-ee-public/downloads/sxc-1.0.9-Darwin.7z"

  sha256 ""

  ## Dependencies
  depends_on "p7zip" => :build

  def install
    system "cp", "-r", "./sxc-1.0.9-Darwin/", "#{prefix}"
  end

  test do
    system "true"
  end
end

__END__

